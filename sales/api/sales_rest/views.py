from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import AutomobileVO, Salesperson, Customer, Sale
from common.json import ModelEncoder
from decimal import Decimal

# Create your views here.


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "sold",
    ]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        return super().default(obj)

    # to make decimal JSON readable


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse({"salespeople": salespeople}, encoder=SalespersonEncoder)
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(salesperson, encoder=SalespersonEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_salespeople(request, pk):
    if request.method == "DELETE":
        count, _ = Salesperson.objects.filter(employee_id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        salesperson = Salesperson.objects.get(employee_id=pk)
        return JsonResponse({"salesperson": salesperson}, encoder=SalespersonEncoder)


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse({"customer": customer}, encoder=CustomerEncoder)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(customer, encoder=CustomerEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_customer(request, pk):
    if request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        customer = Customer.objects.get(id=pk)
        return JsonResponse({"customer": customer}, encoder=CustomerEncoder)


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sale = Sale.objects.all()
        return JsonResponse({"sale": sale}, encoder=SaleEncoder)

    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(import_href=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"Message": "Invalid automobile"}, status=404)
        try:
            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"Message": "Invalid Salesperson"}, status=404)
        try:
            customer = Customer.objects.get(phone_number=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"Message": "Invaild Customer"}, status=404)

        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=SaleEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_sale(request, pk):
    if request.method == "DELETE":
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        sale = Sale.objects.get(id=pk)
        return JsonResponse({"sale": sale}, encoder=SaleEncoder)

        # except AutomobileVO.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response
        # except Salesperson.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response
        # except Customer.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response
