from django.urls import path

from .views import (
    api_list_salespeople,
    api_show_salespeople,
    api_list_customers,
    api_show_customer,
    api_list_sales,
    api_show_sale,
)


urlpatterns = [
    path("salespeople/", api_list_salespeople, name="list_salespeople"),
    path("salespeople/<pk>/", api_show_salespeople, name="show_salesperson"),
    path("customers/", api_list_customers, name="list_customers"),
    path("customers/<int:pk>/", api_show_customer, name="show_customer"),
    path("sale/", api_list_sales, name="list_sales"),
    path("sale/<int:pk>/", api_show_sale, name="show_sale"),
]
