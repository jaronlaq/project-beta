from django.db import models
from django.urls import reverse


# Create your models here.


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True, primary_key=True)

    def get_api_url(self):
        return reverse("show_salesperson", kwargs={"pk": self.pk})

    def __str__(self):
        return self.employee_id


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.TextField()
    phone_number = models.CharField(max_length=20)

    def get_api_url(self):
        return reverse("show_customer", kwargs={"pk": self.pk})

    def __str__(self):
        return self.phone_number


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO, related_name="sales", on_delete=models.CASCADE
    )
    salesperson = models.ForeignKey(
        Salesperson, related_name="sales", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer, related_name="sales", on_delete=models.CASCADE
    )

    price = models.DecimalField(max_digits=10, decimal_places=2)

    def get_api_url(self):
        return reverse("show_sale", kwargs={"pk": self.pk})

    def __str__(self):
        return self.price
