from django.urls import path

from .views import technicians_api, show_technicians_api, appointments_api, show_appointment_api, update_appointment_api

urlpatterns = [
    path("technicians/", technicians_api, name="list_technicians"),
    path("technicians/<int:pk>/", show_technicians_api, name="show_technician"),
    path("appointments/", appointments_api, name="api_appointments"),
    path("appointments/<int:pk>/", show_appointment_api, name="show_appointment"),
    path("appointments/<int:pk>/cancel/", update_appointment_api, name="cancel_appointment"),
    path("appointments/<int:pk>/finish/", update_appointment_api, name="finish_appointment"),
]
