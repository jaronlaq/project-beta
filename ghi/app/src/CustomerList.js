import React, { useEffect, useState } from 'react';

function CustomerList() {

    const [customer, setCustomer] = useState([]);

    async function getCustomers() {
        const customersUrl = 'http://localhost:8090/api/customers/'
        const response = await fetch(customersUrl)
        if (response.ok) {
            const { customer } = await response.json();
            setCustomer(customer)
        } else {
            console.error('Error')
        }
    }

    useEffect(() => {
        getCustomers();
    }, []);

    return (
        <div className="customers-list">
            <div className="customers-details" key={customer.href}>
                < table className="table table-striped" >
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {customer.map((customer) => {
                            return (
                                <tr key={customer.href}>
                                    <td>{customer.first_name}</td>
                                    <td>{customer.last_name}</td>
                                    <td>{customer.address}</td>
                                    <td>{customer.phone_number}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div >
    )
}

export default CustomerList
