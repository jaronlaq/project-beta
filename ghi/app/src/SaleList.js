import React, { useEffect, useState } from 'react';

function SaleList() {

    const [sale, setSale] = useState([]);

    async function getSale() {
        const saleUrl = 'http://localhost:8090/api/sale/'
        const response = await fetch(saleUrl)
        if (response.ok) {
            const { sale } = await response.json();
            setSale(sale)
        } else {
            console.error('Error')
        }
    }

    useEffect(() => {
        getSale();
    }, []);

    return (
        <div className="sale-list">
            <div className="sale-details" key={sale.href}>
                < table className="table table-striped" >
                    <thead>
                        <tr>
                            <th>Automobile</th>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sale.map((sale) => {
                            return (
                                <tr key={sale.automobile.vin}>
                                    <td>{sale.automobile.vin}</td>
                                    <td>{sale.salesperson.first_name} {sale.salesperson.last_name} {sale.salesperson.employee_id} </td>
                                    <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                    <td>${sale.price}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div >
    )
}

export default SaleList
