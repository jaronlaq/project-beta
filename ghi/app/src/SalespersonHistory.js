import React, { useEffect, useState } from 'react';

function SalespersonHistoryList() {

    const [salespeople, setSalespeople] = useState([]);
    const [sale, setSale] = useState([])
    const [selectedSalesperson, setSelectedSalesperson] = useState('');

    async function getSalespeople() {
        const salespersonUrl = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(salespersonUrl)
        if (response.ok) {
            const { salespeople } = await response.json();
            setSalespeople(salespeople)
        } else {
            console.error('Error')
        }
    }

    async function getSale() {
        const saleUrl = 'http://localhost:8090/api/sale/'
        const response = await fetch(saleUrl)
        if (response.ok) {
            const { sale } = await response.json();
            const filteredSale = sale.filter(s => s.salesperson.employee_id === selectedSalesperson)
            setSale(filteredSale)
        } else {
            console.error('Error')
        }
    }
    useEffect(() => {
        getSalespeople();
        getSale();
    }, []);
    const handleSalespersonChange = (event) => {
        const selectedSalespersonId = event.target.value;
        setSelectedSalesperson(selectedSalespersonId);
    }


        useEffect(() => {
            getSalespeople();
            getSale();
        }, [selectedSalesperson]);


        return (
            <div className="saleshistory-list">
                <div className="salespeople-details">
                    <select value={selectedSalesperson} onChange={handleSalespersonChange}>
                        <option value="">Choose a Salesperson</option>
                        {salespeople.map(salesperson => (
                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        ))}
                    </select>
                    < table className="table table-striped" >
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Vin</th>
                                <th>Customer ID</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sale.map((sale) => {
                                return (
                                    <tr key={sale.href}>
                                        <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                        <td>{sale.automobile.vin}</td>
                                        <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                        <td>{sale.price}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div >
        )
    }

export default SalespersonHistoryList
