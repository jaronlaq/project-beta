import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AutomobileForm from './AutomobileForm.js';
import AutomobilesList from './AutomobileList.js';
import ServiceHistory from './ServiceHistory';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import SalespersonForm from './CreateSalesperson.js';
import CustomerForm from './CreateCustomer';
import SalespeopleList from './SalespersonList.js';
import CustomerList from './CustomerList.js';
import SaleForm from './CreateSale.js';
import SaleList from './SaleList.js';
import VehicleModelList from './VehicleModelList.js';
import VehicleModelForm from './CreateVehicleModel.js';
import SalespersonHistoryList from './SalespersonHistory.js';
import ManufacturerForm from './CreateManufacturer.js';
import ManufacturerList from './ManufacturerList.js';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians" element={<TechnicianList />} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="appointments/new" element={<AppointmentForm />} />
          <Route path="appointments" element={<AppointmentList />} />
          <Route path="servicehistory" element={<ServiceHistory />} />
          <Route path="automobiles" element={<AutomobilesList />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path="/create/salesperson" element={<SalespersonForm />} />
          <Route path="/create/customer" element={<CustomerForm />} />
          <Route path="/salespeople" element={<SalespeopleList />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/create/sale" element={<SaleForm />} />
          <Route path="/sales" element={<SaleList />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/create/manufacturers" element={<ManufacturerForm />} />
          <Route path="/vehicle" element={<VehicleModelList />} />
          <Route path="/create/vehicle" element={<VehicleModelForm />} />
          <Route path="/sales/history" element={<SalespersonHistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
