import React, { useEffect, useState } from 'react';

function VehicleModelList() {

    const [models, setModels] = useState([]);

    async function getModels() {
        const modelsUrl = 'http://localhost:8100/api/models/'
        const response = await fetch(modelsUrl)
        if (response.ok) {
            const { models } = await response.json();
            setModels(models)
        } else {
            console.error('Error')
        }
    }

    useEffect(() => {
        getModels();
    }, []);

    return (
        <div className="vehicle-model-list">
            <div className="vehicle-model-details" key={models.href}>
                < table className="table table-striped" >
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {models.map((models) => {
                            return (
                                <tr key={models.href}>
                                    <td>{models.name}</td>
                                    <td>{models.manufacturer.name}</td>
                                    <td><img src={models.picture_url} alt={models.name} style={{ width: '100px', height: '100px' }} /> </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div >
    )
}

export default VehicleModelList
